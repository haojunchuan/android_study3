package com.usts.room_study.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.usts.room_study.db.entity.Student;

import java.util.List;

@Dao
public interface StudentDao {
    @Insert
    void insertStudent(Student... student);

    @Delete
    void deleteStudent(Student student);

    @Update
    void updateStudent(Student student);

    @Query("delete from student")
    void deleteAll();

    @Query("select * from student")
    LiveData<List<Student>> getStudentList();

    @Query("select * from student where uid=:id")
    Student getStudentById(int id);
}
