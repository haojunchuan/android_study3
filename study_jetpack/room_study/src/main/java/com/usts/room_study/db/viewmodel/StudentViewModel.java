package com.usts.room_study.db.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.usts.room_study.db.entity.Student;
import com.usts.room_study.db.repository.StudentRepository;

import java.util.List;

/**
 * author：Jc
 * date: 2021/6/26
 * qq：1562868065
 */
public class StudentViewModel extends AndroidViewModel {
    StudentRepository studentRepository;

    public StudentViewModel(@NonNull  Application application) {
        super(application);
        studentRepository=new StudentRepository(application);
    }

    public LiveData<List<Student>> getAllStu(){
        return studentRepository.getAllStu();
    }

    public void insetstu(Student... students){
        studentRepository.insetstu(students);
    }
}
