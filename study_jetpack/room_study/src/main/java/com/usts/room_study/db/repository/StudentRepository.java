package com.usts.room_study.db.repository;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.usts.room_study.db.MyDatabase;
import com.usts.room_study.db.dao.StudentDao;
import com.usts.room_study.db.entity.Student;

import java.util.List;

/**
 * author：Jc
 * date: 2021/6/26
 * qq：1562868065
 */
public class StudentRepository {
    private LiveData<List<Student>> allStu;
    private StudentDao studentDao;

    public StudentRepository(Context context){
        MyDatabase myDatabase = MyDatabase.getInstance(context);
        studentDao=myDatabase.getStudentDao();
        allStu=studentDao.getStudentList();
    }

    public LiveData<List<Student>> getAllStu(){
        return allStu;
    }

    public void insetstu(Student... students){
        new InsertAsyncTask(studentDao).execute(students);
    }

    public static class InsertAsyncTask extends AsyncTask<Student,Void,Void>{
        private StudentDao studentDao;

        public InsertAsyncTask(StudentDao studentDao){
            this.studentDao=studentDao;
        }

        @Override
        protected Void doInBackground(Student... students) {
            studentDao.insertStudent(students);
            return null;
        }
    }
}
