package com.usts.room_study.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.usts.room_study.db.dao.StudentDao;
import com.usts.room_study.db.entity.Student;

/**
 * author：Jc
 * date: 2021/6/26
 * qq：1562868065
 */
@Database(entities = {Student.class},version = 1,exportSchema = false)
public abstract class MyDatabase extends RoomDatabase {
    private static final String DATABASE_NAME="room_test";
    private static MyDatabase myDatabase;

    //单列模式
    public static synchronized MyDatabase getInstance(Context context){
        if(myDatabase==null){
            myDatabase= Room.databaseBuilder(
                    context.getApplicationContext(),
                    MyDatabase.class,
                    DATABASE_NAME
            )
                    .allowMainThreadQueries()
                    .build();
        }
        return myDatabase;
    }

    public abstract StudentDao getStudentDao();
}
