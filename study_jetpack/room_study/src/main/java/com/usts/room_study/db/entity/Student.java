package com.usts.room_study.db.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

/**
 * author：Jc
 * date: 2021/6/26
 * qq：1562868065
 */
@Entity(tableName = "student")
public class Student {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="uid",typeAffinity = ColumnInfo.INTEGER)
    private int uid;

    @ColumnInfo(name="name",typeAffinity = ColumnInfo.TEXT)
    private String name;

    @ColumnInfo(name="age",typeAffinity = ColumnInfo.INTEGER)
    private int age;

    /**
     * room会默认调用这个构造函数
     * @param uid
     * @param name
     * @param age
     */
    public Student(int uid, String name, int age) {
        this.uid = uid;
        this.name = name;
        this.age = age;
    }

    /**
     * Ignore注解表示room会忽略这个构造函数
     * @param name
     * @param age
     */
    @Ignore
    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Ignore
    @Override
    public String toString() {
        return "Student{" +
                "uid=" + uid +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
