package com.usts.room_study;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;

import com.usts.room_study.databinding.ActivityMainBinding;
import com.usts.room_study.db.entity.Student;
import com.usts.room_study.db.viewmodel.StudentViewModel;

import java.util.List;

/**
 * 该示例是一个viewmodel+livedata+room三个组件结合使用的例子
 * https://blog.csdn.net/XiaoYunKuaiFei/article/details/105639153
 */
public class MainActivity extends AppCompatActivity {
    private final String TAG="MainActivity";
    private ActivityMainBinding binding;
    private StudentViewModel studentViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityMainBinding.inflate(getLayoutInflater());
        View root = binding.getRoot();
        setContentView(root);

        studentViewModel=new ViewModelProvider(this).get(StudentViewModel.class);

        LiveData<List<Student>> allStu = studentViewModel.getAllStu();
        allStu.observe(this, new Observer<List<Student>>() {
            @Override
            public void onChanged(List<Student> students) {
                StringBuilder sb=new StringBuilder();
                for(Student s:students){
                    sb.append(s.toString()+"\n");
                }
                binding.textView2.setText(sb.toString());
            }
        });

       binding.btnAdd.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               String name = binding.inputName.getText().toString();
               String age = binding.inputAge.getText().toString();
               studentViewModel.insetstu(new Student(name,Integer.valueOf(age)));
           }
       });

//        LiveData<List<Student>> studentList = studentDao.getStudentList();
//        studentList.observe(this, new Observer<List<Student>>() {
//            @Override
//            public void onChanged(List<Student> students) {
//                StringBuilder sb=new StringBuilder();
//                for(Student s:students){
//                    sb.append(s.toString()+"\n");
//                }
//                binding.textView2.setText(sb.toString());
//            }
//        });
//
//        binding.btnAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                studentDao.insertStudent(new Student("test33",11));
//            }
//        });
    }




}