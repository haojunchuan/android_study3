package com.usts.viewmodeltest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private MyViewModel myViewModel;
    private TextView tv;
    private Button bt1,bt2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        获取viewmodel方法一：https://www.cnblogs.com/guanxinjing/p/13442423.html
//        MyViewModelFactory myViewModelFactory = new MyViewModelFactory();
//        myViewModel=new ViewModelProvider(this,myViewModelFactory).get(MyViewModel.class);

//        获取viewmodel方法二：https://blog.csdn.net/weixin_42981803/article/details/106156076
//        添加implementation 'androidx.lifecycle:lifecycle-extensions:2.2.0'即可
        myViewModel=new ViewModelProvider(this).get(MyViewModel.class);

        tv=findViewById(R.id.tv);
        bt1=findViewById(R.id.btn1);
        bt2=findViewById(R.id.btn2);

        tv.setText(String.valueOf(myViewModel.num));
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myViewModel.num++;
                tv.setText(String.valueOf(myViewModel.num));
            }
        });

        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myViewModel.num+=2;
                tv.setText(String.valueOf(myViewModel.num));
            }
        });

    }
}