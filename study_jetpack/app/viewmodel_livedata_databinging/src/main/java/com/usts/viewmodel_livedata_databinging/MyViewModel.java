package com.usts.viewmodel_livedata_databinging;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

/**
 * author：Jc
 * date: 2021/6/25
 * qq：1562868065
 */
public class MyViewModel extends ViewModel {
    private MutableLiveData<Integer> num;


    public  MutableLiveData<Integer> getNum(){
        if(num==null){
            num=new MutableLiveData<Integer>();
            num.setValue(0);
        }
        return num;
    }

    public void addNum(Integer n){
        num.setValue(num.getValue()+n);
    }
}
