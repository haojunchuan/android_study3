package com.usts.viewmodel_livedata;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    ViewModelWithLivedata viewModelWithLivedata;
    private TextView tv;
    private ImageButton btLike,btDislike;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewModelWithLivedata=new ViewModelProvider(this).get(ViewModelWithLivedata.class);
        tv=findViewById(R.id.text1);
        btLike=findViewById(R.id.imageButton);
        btDislike=findViewById(R.id.imageButton2);
//        添加观察
        viewModelWithLivedata.getLikeNum().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                tv.setText(String.valueOf(integer));
            }
        });

        btLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModelWithLivedata.setLikeNum(1);
            }
        });
        btDislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModelWithLivedata.setLikeNum(-1);
            }
        });
    }
}