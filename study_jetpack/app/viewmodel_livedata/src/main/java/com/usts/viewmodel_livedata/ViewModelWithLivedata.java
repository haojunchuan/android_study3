package com.usts.viewmodel_livedata;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

/**
 * author：Jc
 * date: 2021/6/25
 * qq：1562868065
 */
public class ViewModelWithLivedata extends ViewModel {
    private MutableLiveData<Integer> likeNum;

    public MutableLiveData<Integer> getLikeNum() {
        if(likeNum==null){
            likeNum=new MutableLiveData<Integer>();
            likeNum.setValue(0);
        }
        return likeNum;
    }

    public void setLikeNum(Integer n) {
        likeNum.setValue(likeNum.getValue()+n);
    }
}
