package com.study.ijkplayer_study;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import cn.jzvd.Jzvd;
import cn.jzvd.JzvdStd;

public class JiaoziVideoPlayerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jiaozi_video_player);

        JzvdStd jzvdStd =  findViewById(R.id.jz_video);
        jzvdStd.setUp("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"
                , "测试");
    }

    @Override
    public void onBackPressed() {
        if (Jzvd.backPress()) {
            return;
        }
        super.onBackPressed();
    }
    @Override
    protected void onPause() {
        super.onPause();
        Jzvd.releaseAllVideos();
    }
}