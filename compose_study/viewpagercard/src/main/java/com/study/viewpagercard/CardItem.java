package com.study.viewpagercard;

/**
 * author : Jc
 * date   : 2021/6/27
 * weichat:haojundewexin
 */
public class CardItem {

    private int mTextResource;
    private int mTitleResource;

    public CardItem(int title, int text) {
        mTitleResource = title;
        mTextResource = text;
    }

    public int getText() {
        return mTextResource;
    }

    public int getTitle() {
        return mTitleResource;
    }
}