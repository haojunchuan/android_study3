package com.usts.gsy_videoplayer_study;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.shuyu.gsyvideoplayer.listener.GSYVideoShotListener;
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;
import com.usts.gsy_videoplayer_study.utile.NoteItem;
import com.usts.gsy_videoplayer_study.utile.NoteRecycleViewAdapter;
import com.usts.gsy_videoplayer_study.utile.SaveAndGetPhoto;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * author：Jc
 * date: 2021/7/9
 * qq：1562868065
 */
public class JcPlayer extends StandardGSYVideoPlayer {
    private Context context;
//    private TextView testText;
//    弹幕是否开启
    private Boolean mDanmuIsOn=false;
//    弹幕按钮
    private ImageView mDanmuBtn;
//    调整播放速度按钮
    private TextView mBntChangePlaySpeed;
//    是否显示调速框
    private boolean mIsShowChangePlaySpeedDialog=false;
//    调速框对象
    private Dialog mChangePlaySpeedDialog;
//    当前速度
    private int currSpeedIndex=1;
//    改变播放速度的所有按钮
    private List<TextView> mBtnsChangePlayspeed=new ArrayList<>();
//    截屏按钮
    private LinearLayout shotScreenView;
//    截屏按钮
    private ImageView shootScreenBtn;
//    展示笔记按钮
    private TextView showNotePicturesBtn;
//    展示笔记的Dialog
    private Dialog mShowNoteDialog;

//    笔记数据
    private List<NoteItem> notes;
    public JcPlayer(Context context, Boolean fullFlag) {
        super(context, fullFlag);
        this.context=context;
    }

    public JcPlayer(Context context) {
        super(context);
        this.context=context;
    }

    public JcPlayer(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
    }

    @Override
    public int getLayoutId() {
        return R.layout.video_layout_cover;
    }

    @Override
    protected int getVolumeLayoutId() {
        return R.layout.video_volum_dialog_cover;
    }

//    重写初始方法，可添加自己按钮的逻辑
    @Override
    protected void init(Context context) {
        super.init(context);
        mDanmuBtn=findViewById(R.id.danmu_controller_btn);
        mBntChangePlaySpeed=findViewById(R.id.video_change_playspeed_btn);
        shotScreenView=findViewById(R.id.shot_screen_btns);
        shootScreenBtn=findViewById(R.id.player_btn_camare);
        showNotePicturesBtn=findViewById(R.id.player_show_notes_btn);

        notes=new ArrayList<>();

//        处理弹幕按你逻辑
        danmuController(mDanmuBtn);
//        调整播放按钮
        changePlaySpeed(mBntChangePlaySpeed);
//        截屏
        shotScreenAndSave(shootScreenBtn);
//        展示笔记
        showNote(showNotePicturesBtn);

    }

//    展示笔记
    protected void showNote(TextView showNotePicturesBtn){
        showNotePicturesBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showAllNotePictureList();
            }
        });
    }

//    截屏逻辑
    protected void shotScreenAndSave(ImageView shootScreenBtn){
        shootScreenBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("TAG", "onClick: ++++++++++++++++++++++++++++++++++++++++++++++++++++");
                taskShotPic(new GSYVideoShotListener() {
                    @Override
                    public void getBitmap(Bitmap bitmap) {
                        long currentTimeMillis = System.currentTimeMillis();
                        String fileName=currentTimeMillis+".png";
                        SaveAndGetPhoto.saveBitmap(context,bitmap,fileName);
                        Toast.makeText(context,"创建笔记成功！" ,Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "getBitmap: "+getCurrentPositionWhenPlaying());
                    }
                },true);
            }
        });
    }

//    响应调整播放速度按钮
    protected void changePlaySpeed(TextView mBntChangePlaySpeed){
        mBntChangePlaySpeed.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showChangePlaySpeedDialog(currSpeedIndex);
            }
        });
    }
//    弹幕控制器
    protected void danmuController(ImageView mDanmuBtn){
        mDanmuBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!mDanmuIsOn){//如果没开启
                    mDanmuBtn.setImageResource(R.drawable.ic_danmu_on);
                    mDanmuIsOn=!mDanmuIsOn;
                }else{//如果已经开启
                    mDanmuBtn.setImageResource(R.drawable.ic_danmu_close);
                    mDanmuIsOn=!mDanmuIsOn;
                }
            }
        });
    }

//    重写缓冲时界面元素显示方法，解决缓冲时播放按钮消失的问题
    @Override
    protected void changeUiToPlayingBufferingShow() {
        super.changeUiToPlayingBufferingShow();
        setViewShowState(mStartButton, VISIBLE);
        setViewShowState(shotScreenView, VISIBLE);
    }

//    解决重新开始播放时，播放按钮消失问题
    @Override
    protected void changeUiToPreparingShow() {
        super.changeUiToPreparingShow();
        setViewShowState(mStartButton, VISIBLE);
        setViewShowState(shotScreenView, VISIBLE);
    }

    /**
     * 重写-触摸显示滑动进度dialog
     */
    @Override
    protected void showProgressDialog(float deltaX, String seekTime, int seekTimePosition, String totalTime, int totalTimeDuration) {
        if (mProgressDialog == null) {
            View localView = LayoutInflater.from(getActivityContext()).inflate(getProgressDialogLayoutId(), null);
            if (localView.findViewById(getProgressDialogProgressId()) instanceof ProgressBar) {
                mDialogProgressBar = ((ProgressBar) localView.findViewById(getProgressDialogProgressId()));
                if (mDialogProgressBarDrawable != null) {
                    mDialogProgressBar.setProgressDrawable(mDialogProgressBarDrawable);
                }
            }
            if (localView.findViewById(getProgressDialogCurrentDurationTextId()) instanceof TextView) {
                mDialogSeekTime = ((TextView) localView.findViewById(getProgressDialogCurrentDurationTextId()));
            }
            if (localView.findViewById(getProgressDialogAllDurationTextId()) instanceof TextView) {
                mDialogTotalTime = ((TextView) localView.findViewById(getProgressDialogAllDurationTextId()));
            }
            if (localView.findViewById(getProgressDialogImageId()) instanceof ImageView) {
                mDialogIcon = ((ImageView) localView.findViewById(getProgressDialogImageId()));
            }
            mProgressDialog = new Dialog(getActivityContext(), R.style.video_style_dialog_progress);
            mProgressDialog.setContentView(localView);
            mProgressDialog.getWindow().addFlags(Window.FEATURE_ACTION_BAR);
            mProgressDialog.getWindow().addFlags(32);
            mProgressDialog.getWindow().addFlags(16);
            mProgressDialog.getWindow().setLayout(getWidth(), getHeight());
            if (mDialogProgressNormalColor != -11 && mDialogTotalTime != null) {
                mDialogTotalTime.setTextColor(mDialogProgressNormalColor);
            }
            if (mDialogProgressHighLightColor != -11 && mDialogSeekTime != null) {
                mDialogSeekTime.setTextColor(mDialogProgressHighLightColor);
            }
            WindowManager.LayoutParams localLayoutParams = mProgressDialog.getWindow().getAttributes();
            localLayoutParams.gravity = Gravity.TOP;
            localLayoutParams.width = getWidth();
            localLayoutParams.height = getHeight();
            int location[] = new int[2];
            getLocationOnScreen(location);
            localLayoutParams.x = location[0];
            localLayoutParams.y = location[1];
            mProgressDialog.getWindow().setAttributes(localLayoutParams);
        }
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
        if (mDialogSeekTime != null) {
            mDialogSeekTime.setText(seekTime);
        }
        if (mDialogTotalTime != null) {
            mDialogTotalTime.setText(" / " + totalTime);
        }
        if (totalTimeDuration > 0)
            if (mDialogProgressBar != null) {
                mDialogProgressBar.setProgress(seekTimePosition * 100 / totalTimeDuration);
            }

        taskShotPic(new GSYVideoShotListener() {
            @Override
            public void getBitmap(Bitmap bitmap) {
                mDialogIcon.setImageBitmap(bitmap);
            }
        },true);

//        if (deltaX > 0) {
//            if (mDialogIcon != null) {
//                mDialogIcon.setBackgroundResource(R.drawable.video_forward_icon);
//            }
//        } else {
//            if (mDialogIcon != null) {
//                mDialogIcon.setBackgroundResource(R.drawable.video_backward_icon);
//            }
//        }

    }

    @Override
    protected void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

//    展示笔记的Dialog
    private void showAllNotePictureList(){
        //        先隐藏原先的控制面板
        changeUiToPlayingClear();
        if (mShowNoteDialog == null) {
            View localView = LayoutInflater.from(getActivityContext()).inflate(R.layout.video_show_study_note_picture_dialog, null);
            mShowNoteDialog = new Dialog(getActivityContext(),R.style.change_playspeed_style);
            mShowNoteDialog.setContentView(localView);
            mShowNoteDialog.getWindow().addFlags(Window.FEATURE_ACTION_BAR);
//            mChangePlaySpeedDialog.getWindow().addFlags(32);
//            mChangePlaySpeedDialog.getWindow().addFlags(16);
            mShowNoteDialog.getWindow().setLayout(getWidth(), getHeight());

            WindowManager.LayoutParams localLayoutParams = mShowNoteDialog.getWindow().getAttributes();
            localLayoutParams.gravity = Gravity.TOP;
            localLayoutParams.width = getWidth();
            localLayoutParams.height = getHeight();
            int location[] = new int[2];
            getLocationOnScreen(location);
            localLayoutParams.x = location[0];
            localLayoutParams.y = location[1];
            mShowNoteDialog.getWindow().setAttributes(localLayoutParams);

            //触摸任意位置让面板消失
            View viewById = localView.findViewById(R.id.container_show_note);
            viewById.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    mShowNoteDialog.dismiss();
                    return false;
                }
            });

//            ImageView cImg = localView.findViewById(R.id.);
//            SaveAndGetPhoto.getBitmap(context,"",cImg);
            String TargetPath = mContext.getFilesDir() + "/images/1626172281704";
            for (int i=1;i<5;i++){
                notes.add(new NoteItem(i,"2:30","哈哈哈哈","2021-2-2",SaveAndGetPhoto.getBitmap(TargetPath)));
            }
            RecyclerView noteRecycleView = localView.findViewById(R.id.note_picture_list_rc);
            NoteRecycleViewAdapter noteRecycleViewAdapter = new NoteRecycleViewAdapter(notes);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            noteRecycleView.setLayoutManager(linearLayoutManager);
            noteRecycleView.setAdapter(noteRecycleViewAdapter);
        }


        mShowNoteDialog.show();
    }

//展示播放速度面板
    protected void showChangePlaySpeedDialog(int currSpeed){
//        先隐藏原先的控制面板
        changeUiToPlayingClear();
//         localView;
        if (mChangePlaySpeedDialog == null) {
            View localView = LayoutInflater.from(getActivityContext()).inflate(R.layout.video_change_play_speed_dialog, null);
            mChangePlaySpeedDialog = new Dialog(getActivityContext(),R.style.change_playspeed_style);
            mChangePlaySpeedDialog.setContentView(localView);
            mChangePlaySpeedDialog.getWindow().addFlags(Window.FEATURE_ACTION_BAR);
//            mChangePlaySpeedDialog.getWindow().addFlags(32);
//            mChangePlaySpeedDialog.getWindow().addFlags(16);
            mChangePlaySpeedDialog.getWindow().setLayout(getWidth(), getHeight());

            WindowManager.LayoutParams localLayoutParams = mChangePlaySpeedDialog.getWindow().getAttributes();
            localLayoutParams.gravity = Gravity.TOP;
            localLayoutParams.width = getWidth();
            localLayoutParams.height = getHeight();
            int location[] = new int[2];
            getLocationOnScreen(location);
            localLayoutParams.x = location[0];
            localLayoutParams.y = location[1];
            mChangePlaySpeedDialog.getWindow().setAttributes(localLayoutParams);

            //触摸任意位置让面板消失
            View viewById = localView.findViewById(R.id.container_change_playspeed);
            viewById.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    mChangePlaySpeedDialog.dismiss();
                    return false;
                }
            });


        //0.75倍速
            TextView btn_0_75 = localView.findViewById(R.id.play_speed_0_75x);
            mBtnsChangePlayspeed.add(btn_0_75);
            btn_0_75.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setSpeed(0.75f);
                currSpeedIndex=0;
                mChangePlaySpeedDialog.dismiss();
            }
        });


            //1.0倍速
            TextView btn_1_0 = localView.findViewById(R.id.play_speed_1_0x);
            mBtnsChangePlayspeed.add(btn_1_0);
            btn_1_0.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSpeed(1.0f);
                    currSpeedIndex=1;
                    mChangePlaySpeedDialog.dismiss();
                }
            });

            //1.25倍速
            TextView btn_1_25 = localView.findViewById(R.id.play_speed_1_25x);
            mBtnsChangePlayspeed.add(btn_1_25);
            btn_1_25.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSpeed(1.25f);
                    currSpeedIndex=2;
                    mChangePlaySpeedDialog.dismiss();
                }
            });

            //1.5倍速
            TextView btn_1_5 = localView.findViewById(R.id.play_speed_1_5x);
            mBtnsChangePlayspeed.add(btn_1_5);
            btn_1_5.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSpeed(1.5f);
                    currSpeedIndex=3;
                    mChangePlaySpeedDialog.dismiss();
                }
            });

            //1.75倍速
            TextView btn_1_75 = localView.findViewById(R.id.play_speed_1_75x);
            mBtnsChangePlayspeed.add(btn_1_75);
            btn_1_75.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSpeed(1.75f);
                    currSpeedIndex=4;
                    mChangePlaySpeedDialog.dismiss();
                }
            });

        }
        changPlayspeedBtnColor(mBtnsChangePlayspeed,currSpeedIndex);
        mChangePlaySpeedDialog.show();
    }

//    改变播放速度按钮的颜色
    protected void changPlayspeedBtnColor(List<TextView> mBtnsChangePlayspeed,int currIndex){
        for(TextView textView:mBtnsChangePlayspeed){
            textView.setTextColor(getResources().getColor(R.color.white));
//            清除Background
//            textView.setBackgroundResource(0);

        }
//        更改颜色
        mBtnsChangePlayspeed.get(currIndex).setTextColor(getResources().getColor(R.color.style_color));
//        android:background="@drawable/background_lineout_play_speed"
//        添加背景框
//        mBtnsChangePlayspeed.get(currIndex).setBackground(getResources().getDrawable(R.drawable.background_lineout_play_speed));
    }

//    修改锁屏的图标
    @Override
    protected void lockTouchLogic() {
        if (mLockCurScreen) {
            mLockScreen.setImageResource(R.drawable.ic_unlock);
            mLockCurScreen = false;
        } else {
            mLockScreen.setImageResource(R.drawable.ic_lock);
            mLockCurScreen = true;
            hideAllWidget();
        }
    }

    @Override
    protected void hideAllWidget() {
        super.hideAllWidget();
        setViewShowState(shotScreenView, INVISIBLE);
    }

    @Override
    protected void changeUiToNormal() {
        super.changeUiToNormal();
        setViewShowState(shotScreenView, INVISIBLE);
    }

    @Override
    protected void changeUiToPlayingShow() {
        super.changeUiToPlayingShow();
        setViewShowState(shotScreenView, VISIBLE);
    }

    @Override
    protected void changeUiToPauseShow() {
        super.changeUiToPauseShow();
        setViewShowState(shotScreenView, VISIBLE);
    }

    @Override
    protected void changeUiToCompleteShow() {
        super.changeUiToCompleteShow();
        setViewShowState(shotScreenView, VISIBLE);
    }

    @Override
    protected void changeUiToCompleteClear() {
        super.changeUiToCompleteClear();
        setViewShowState(shotScreenView, INVISIBLE);
    }

    @Override
    protected void changeUiToPlayingClear() {
        super.changeUiToPlayingClear();
        setViewShowState(shotScreenView, INVISIBLE);
    }

    @Override
    protected void changeUiToPauseClear() {
        super.changeUiToPauseClear();
        setViewShowState(shotScreenView, INVISIBLE);
    }
}
