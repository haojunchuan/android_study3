package com.usts.gsy_videoplayer_study;

import android.app.Application;
import android.content.Context;

/**
 * author : Jc
 * date   : 2021/7/13
 * weichat:haojundewexin
 */
public class MyApplication extends Application {
    private static final String TAG = "MyApplication";
    private Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

}
