package com.usts.gsy_videoplayer_study;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.listener.GSYVideoShotListener;
import com.shuyu.gsyvideoplayer.utils.OrientationUtils;
import com.squareup.picasso.Picasso;

public class CustomerTest1Activity extends AppCompatActivity {
    private JcPlayer jcPlayer;
    private OrientationUtils orientationUtils;

//    private ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); // 隐藏标题 //这行代码一定要在setContentView之前，不然会闪退
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_customer_test1);

//        img=findViewById(R.id.img);
        init();

    }

    protected void init(){
        jcPlayer=findViewById(R.id.my_palyer);
        String source="http://vfx.mtime.cn/Video/2019/03/09/mp4/190309153658147087.mp4";
        jcPlayer.setUp(source,true,"测试视频");
//
        jcPlayer.setNeedLockFull(true);//是否需要全屏锁屏
//        jcPlayer.setNeedShowWifiTip(needShowWifiTip);//非wifi环境下，显示流量提醒,默认为true
//        jcPlayer.setUp()
//        jcPlayer.setSpeed(0.5f);//视频播放速度
        jcPlayer.getTitleTextView().setVisibility(View.VISIBLE);
        jcPlayer.getBackButton().setVisibility(View.VISIBLE);
        jcPlayer.setIsTouchWiget(true);
//        设置全屏
        jcPlayer.setIfCurrentIsFullscreen(true);
//        设置锁屏功能（要配合全屏设置才能生效）
        jcPlayer.setNeedLockFull(true);
        jcPlayer.startPlayLogic();
    }

    @Override
    protected void onPause() {
        super.onPause();
        jcPlayer.onVideoPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        jcPlayer.onVideoResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GSYVideoManager.releaseAllVideos();
        if(orientationUtils!=null){
            orientationUtils.releaseListener();
        }
    }

    @Override
    public void onBackPressed() {

        if(orientationUtils.getScreenType()== ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE){
            jcPlayer.getFullscreenButton().performClick();
            return;
        }
        jcPlayer.setVideoAllCallBack(null);
        super.onBackPressed();
    }

    public void onclick(View view) {
        switch (view.getId()){
//            case R.id.shot_screen:
//                jcPlayer.taskShotPic(new GSYVideoShotListener() {
//                    @Override
//                    public void getBitmap(Bitmap bitmap) {
//                       img.setImageBitmap(bitmap);
//                    }
//                },true);
//                break;
        }
    }


}