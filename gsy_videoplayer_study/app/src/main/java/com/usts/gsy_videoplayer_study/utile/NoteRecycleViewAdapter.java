package com.usts.gsy_videoplayer_study.utile;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.usts.gsy_videoplayer_study.R;

import java.util.List;

/**
 * author : Jc
 * date   : 2021/7/13
 * weichat:haojundewexin
 */
public class NoteRecycleViewAdapter extends RecyclerView.Adapter<NoteRecycleViewAdapter.ViewHolder> {
    private static final String TAG = "NoteRecycleViewAdapter";
    private List<NoteItem> notes;

    public NoteRecycleViewAdapter(List<NoteItem> notes){
        this.notes=notes;
    }

    @NonNull
    @Override
    public NoteRecycleViewAdapter.ViewHolder onCreateViewHolder(@NonNull  ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_note_picture, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NoteRecycleViewAdapter.ViewHolder holder, int position) {
            holder.tvShootScreenTime.setText(notes.get(position).getShootScreenTime());
            holder.tvCustomerNote.setText(notes.get(position).getCustomerNote());
            holder.tvNoteCraetedTime.setText(notes.get(position).getCreatedNoteTime());
            holder.notePic.setImageBitmap(notes.get(position).getPicBitmap());
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvShootScreenTime,tvCustomerNote,tvNoteCraetedTime;
        ImageView notePic;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvShootScreenTime=itemView.findViewById(R.id.shot_time_tv);
            tvCustomerNote=itemView.findViewById(R.id.customer_note_tv);
            tvNoteCraetedTime=itemView.findViewById(R.id.save_note_date_tv);
            notePic=itemView.findViewById(R.id.customer_note_pic);
        }
    }
}
