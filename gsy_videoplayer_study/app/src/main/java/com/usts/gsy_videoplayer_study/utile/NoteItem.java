package com.usts.gsy_videoplayer_study.utile;

import android.graphics.Bitmap;

/**
 * author : Jc
 * date   : 2021/7/13
 * weichat:haojundewexin
 */
public class NoteItem {
    private static final String TAG = "NoteItem";
    private int id;
    private String shootScreenTime;
    private String customerNote;
    private String createdNoteTime;
    private Bitmap picBitmap;

    public NoteItem(int id, String shootScreenTime, String customerNote, String createdNoteTime, Bitmap picBitmap) {
        this.id = id;
        this.shootScreenTime = shootScreenTime;
        this.customerNote = customerNote;
        this.createdNoteTime = createdNoteTime;
        this.picBitmap = picBitmap;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShootScreenTime() {
        return shootScreenTime;
    }

    public void setShootScreenTime(String shootScreenTime) {
        this.shootScreenTime = shootScreenTime;
    }

    public String getCustomerNote() {
        return customerNote;
    }

    public void setCustomerNote(String customerNote) {
        this.customerNote = customerNote;
    }

    public String getCreatedNoteTime() {
        return createdNoteTime;
    }

    public void setCreatedNoteTime(String createdNoteTime) {
        this.createdNoteTime = createdNoteTime;
    }

    public Bitmap getPicBitmap() {
        return picBitmap;
    }

    public void setPicBitmap(Bitmap picBitmap) {
        this.picBitmap = picBitmap;
    }
}
