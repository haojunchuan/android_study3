# 一些文章

1. 使用GSYVideoPlayer增加显示实时网速：https://www.jianshu.com/p/cd1bd1ba09cf
2. Observable.interval可以执行定时任务：https://blog.csdn.net/u013750244/article/details/105998679
3. AlertDialog最常用的使用场景，及各个场景的使用方式: https://blog.csdn.net/qq_40716430/article/details/105968548

# 一、Okhttp

## 1、使用viewbinding

1. 修改模块的build.gradle文件

   ```groovy
   在android中添加
   buildFeatures{
           viewBinding true
       }
   ```

2. 在activity中使用

   ```java
   public class MainActivity extends AppCompatActivity {
       private ActivityMainBinding mBinding;
   
       @Override
       protected void onCreate(Bundle savedInstanceState) {
           super.onCreate(savedInstanceState);
           mBinding=ActivityMainBinding.inflate(getLayoutInflater());
           setContentView(mBinding.getRoot());
   
           mBinding.btn1.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
   
               }
           });
       }
   }
   ```

   

## 2、okhttp基本使用

1. 引入依赖

   ```groovy
   implementation 'com.squareup.okhttp3:okhttp:3.10.0'
   ```

2. http请求需要在application标签中添加

   ```groovy
   android:usesCleartextTraffic="true"
   ```

3. get同步请求

   ```java
   public class MainActivity extends AppCompatActivity {
       private ActivityMainBinding mBinding;
       private OkHttpClient okHttpClient;
   
       @Override
       protected void onCreate(Bundle savedInstanceState) {
           super.onCreate(savedInstanceState);
           mBinding=ActivityMainBinding.inflate(getLayoutInflater());
           setContentView(mBinding.getRoot());
   
           okHttpClient=new OkHttpClient.Builder().build();
   
           mBinding.btn1.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   testGet();
               }
           });
       }
   
       public void testGet(){
           Request request = new Request.Builder()
                   .url("http://192.168.43.148/bookstore/api/v1/books/1")
                   .get()
                   .build();
   
   //        同步请求
           new Thread(new Runnable() {
               @Override
               public void run() {
                   try {
                       Response response = okHttpClient.newCall(request).execute();
                       String result = response.body().string();
                       runOnUiThread(new Runnable() {
                           @Override
                           public void run() {
                               mBinding.tv1.setText(result);
                               Log.i("test", result);
                           }
                       });
                   } catch (IOException e) {
                       e.printStackTrace();
                   }
               }
           }).start();
       }
   }
   ```

4. get异步请求

   ```
   public class MainActivity extends AppCompatActivity {
       private ActivityMainBinding mBinding;
       private OkHttpClient okHttpClient;
   
       @Override
       protected void onCreate(Bundle savedInstanceState) {
           super.onCreate(savedInstanceState);
           mBinding=ActivityMainBinding.inflate(getLayoutInflater());
           setContentView(mBinding.getRoot());
   
           okHttpClient=new OkHttpClient.Builder().build();
   
           mBinding.btn1.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   testGet();
               }
           });
       }
   
       public void testGet(){
           Request request = new Request.Builder()
                   .url("http://192.168.43.148/bookstore/api/v1/books/1")
                   .get()
                   .build();
   
   //        异步请求
           okHttpClient.newCall(request).enqueue(new Callback() {
   //            失败回调
               @Override
               public void onFailure(Call call, IOException e) {
   
               }
   //            成功回调
               @Override
               public void onResponse(Call call, Response response) throws IOException {
                   String result = response.body().string();
                   runOnUiThread(new Runnable() {
                       @Override
                       public void run() {
                           mBinding.tv1.setText(result);
                       }
                   });
               }
           });
       }
   }
   ```

5. post异步请求

   ```java
   public class MainActivity extends AppCompatActivity {
       private ActivityMainBinding mBinding;
       private OkHttpClient okHttpClient;
   
       @Override
       protected void onCreate(Bundle savedInstanceState) {
           super.onCreate(savedInstanceState);
           mBinding=ActivityMainBinding.inflate(getLayoutInflater());
           setContentView(mBinding.getRoot());
   
           okHttpClient=new OkHttpClient.Builder().build();
   
           mBinding.btn1.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   testPost();
               }
   
           });
       }
   
       public void testPost(){
           
           FormBody formBody = new FormBody.Builder()
                   .add("title", "test1")
                   .add("auther", "test")
                   .add("price", "10")
                   .build();
   
           Request request = new Request.Builder()
                   .url("http://192.168.43.148//bookstore/api/v1/books/")
                   .post(formBody)
                   .build();
   
           //        异步请求
           okHttpClient.newCall(request).enqueue(new Callback() {
               //            失败回调
               @Override
               public void onFailure(Call call, IOException e) {
   
               }
               //            成功回调
               @Override
               public void onResponse(Call call, Response response) throws IOException {
                   String result = response.body().string();
                   runOnUiThread(new Runnable() {
                       @Override
                       public void run() {
                           mBinding.tv1.setText(result);
                       }
                   });
               }
           });
       }
   }
   ```

   

# 二、LitePal

## 1、基本使用

1. 引入依赖

   ```groovy
   implementation 'org.litepal.android:java:3.0.0'
   ```

2. 配置

   ```java
   1、在assets中新建litepal.xml
   <?xml version="1.0" encoding="utf-8"?>
   <litepal>
       <dbname value="BookStore"></dbname>
       <version value="1"></version>
       <list>
       </list>
   </litepal>
   
   2、新建自己的application对象初始化litepal
   public class MyApplication extends Application {
       private static Context context;
   
       @Override
       public void onCreate() {
           super.onCreate();
           context=getApplicationContext();
           LitePal.initialize(this);
       }
   
       public Context getContext(){
           return context;
       }
   }
   
   3、在清单文件文件中配置自己的application
   android:name=".MyApplication"
   
   ```

3. 创建实体类映射

   ```
   1、创建bean类，继承LitePalSupport
   public class Book extends LitePalSupport {
       private int id;
       private String name;
       private String author;
   
       public Book(int id, String name, String author) {
           this.id = id;
           this.name = name;
           this.author = author;
       }
   
       public int getId() {
           return id;
       }
   
       public void setId(int id) {
           this.id = id;
       }
   
       public String getName() {
           return name;
       }
   
       public void setName(String name) {
           this.name = name;
       }
   
       public String getAuthor() {
           return author;
       }
   
       public void setAuthor(String author) {
           this.author = author;
       }
   
       @Override
       public String toString() {
           return "Book{" +
                   "id=" + id +
                   ", name='" + name + '\'' +
                   ", author='" + author + '\'' +
                   '}';
       }
   }
   
   2、在litepal.xml中添加映射
   <?xml version="1.0" encoding="utf-8"?>
   <litepal>
       <dbname value="BookStore"></dbname>
       <version value="1"></version>
       <list>
       	<mapping class="com.usts.litepal_study.Book"/>
       </list>
   </litepal>
   ```

4. 曾删改查操作

   ```java
   1、增加一条数据
       Book book = new Book(3, "test1", "test2");
       book.save();
   
   2、删除一条数据
       LitePal.delete(Book.class, 1);
   	或
       LitePal.deleteAll(Book.class, "id > ?" , "1");
   
   3、更新
       更新一条
       Book book = LitePal.find(Book.class, 2);
                   book.setAuthor("JayJay");
                   book.save();
   
   	
   4、查询
       LitePal.find(Class, long id)这个方法前面已经多次使用，它返回一个T对象
   	LitePal.findAll(Class)返回所有对象，即返回表中所有数据
   	我们也可以进行复杂的查询
   	List<Song> songs = LitePal.where("name like ? and duration < ?", "song%", 			"200").order("duration").find(Song.class);
   	如上段代码 .where代表查询的条件，依旧使用占位符的形式,并且支持sql中的通配符 如% _ 等
   	.order代表了排序 我们也可以在order中跟上asc或者desc表示升序或降序
   	order("age desc")	
   ```

5. 升级数据库

   ```
   只需修改配置文件中的version值
   <version value="2"></version>
   ```

   

# 三、Animation动画

1. 图片加载库准备

   ```groovy
   implementation 'com.squareup.picasso:picasso:2.5.2'
   ```

2. 代码编写

   ```
   public class MainActivity extends AppCompatActivity {
       private ImageView img;
   
       @Override
       protected void onCreate(Bundle savedInstanceState) {
           super.onCreate(savedInstanceState);
           setContentView(R.layout.activity_main);
   
           img=findViewById(R.id.imageView);
           String imgUrl="https://acg.toubiec.cn/random.php?t="+Math.random();
           Picasso
                   .with(this)
                   .load(imgUrl)
                   .into(img);
       }
   
       public void onclick(View view) {
   //        picasso图片加载
           String imgUrl="https://acg.toubiec.cn/random.php?t="+Math.random();
           Picasso
                   .with(this)
                   .load(imgUrl)
                   .into(img);
   
           ScaleAnimation animation = new ScaleAnimation(1, 1.5f, 1, 1.5f, Animation.RELATIVE_TO_SELF, 0.5f, 1, 0.5f);
           // 设置持续时间
           animation.setDuration(4000);
           // 设置动画结束之后的状态是否是动画的最终状态
           animation.setFillAfter(true);
           // 设置循环次数
           animation.setRepeatCount(0);
           //开始动画 
           img.startAnimation(animation);
       }
   }
   ```

# 四、Room

## 一、基础使用

1. 依赖

   ```groovy
   def room_version = "2.3.0"
       implementation "androidx.room:room-runtime:$room_version"
       annotationProcessor "androidx.room:room-compiler:$room_version"
   ```

   

2. entity

   ```java
   @Entity(tableName = "user")
   public class User {
       @PrimaryKey(autoGenerate = true)
       public Integer uid;
   
       @ColumnInfo(name="first_name")
       public String firstName;
   
       @ColumnInfo(name="last_name")
       public String lastName;
   
   //    public User(){}
   
       public User(Integer uid, String firstName, String lastName) {
           this.uid = uid;
           this.firstName = firstName;
           this.lastName = lastName;
       }
   
       @Ignore
       public User(String firstName, String lastName) {
           this.firstName = firstName;
           this.lastName = lastName;
       }
   
       @Ignore
       public User(Integer uid) {
           this.uid = uid;
       }
   
       @Ignore
       @Override
       public String toString() {
           return "User{" +
                   "uid=" + uid +
                   ", firstName='" + firstName + '\'' +
                   ", lastName='" + lastName + '\'' +
                   '}';
       }
   }
   ```

   

3. dao

   ```java
   @Dao
   public interface UserDao {
       @Query("select * from user")
       List<User> getAll();
   
       @Query("SELECT * FROM user WHERE uid IN (:userIds)")
       List<User> loadAllByIds(int[] userIds);
   
       @Query("SELECT * FROM user WHERE first_name LIKE :first AND " +
               "last_name LIKE :last LIMIT 1")
       User findByName(String first, String last);
   
       @Insert
       void insertAll(User... users);
   
       @Delete
       void delete(User user);
   }
   ```

   

4. database（单例模式）

   ```java
   @Database(entities = {User.class},version = 2,exportSchema = false)
   public abstract class MyDataBase extends RoomDatabase {
       private static MyDataBase mdb;
       private final static String DB_NAME="test1";
   
       public static synchronized MyDataBase getInstance(Context context){
           if(mdb==null){
               mdb= Room
                       .databaseBuilder(context.getApplicationContext(),MyDataBase.class,DB_NAME)
                       .allowMainThreadQueries()  //允许在主线程中操作
                       .fallbackToDestructiveMigration()  //强制升级
                       .build();
           }
   
           return mdb;
       }
   
       public abstract UserDao getUserDao();
   }
   ```

   5. CRUD

      ```java
      public class MainActivity extends AppCompatActivity {
          private MyDataBase mDb;
          private TextView tv;
          private UserDao userDao;
      
          @Override
          protected void onCreate(Bundle savedInstanceState) {
              super.onCreate(savedInstanceState);
              setContentView(R.layout.activity_main);
      
              mDb=MyDataBase.getInstance(this);
              userDao=mDb.getUserDao();
              tv=findViewById(R.id.tv);
      
          }
      
          public void onclick(View view) {
              switch (view.getId()){
                  case R.id.btn_insert:
                      User user = new User("test1", "test1");
                      userDao.insertAll(user);
                      break;
                  case R.id.btn_select:
                      List<User> all = userDao.getAll();
                      StringBuilder stringBuilder=new StringBuilder();
                      for(User u:all){
                          stringBuilder.append(u.toString());
                      }
                      runOnUiThread(new Runnable() {
                          @Override
                          public void run() {
                              tv.setText(stringBuilder.toString());
                          }
                      });
                      break;
                  case R.id.btn_delete:
                          userDao.deleteAll();
                      break;
      
                  case R.id.btn_select_byid:
                      User u = userDao.getById(1);
                      tv.setText(u.toString());
                      break;
      
              }
          }
      }
      ```

      



# 五、GSYVideoPlayer

GSYVideoPlayer官网：https://github.com/CarGuo/GSYVideoPlayer

## 1、GSYVideoPlayer简单使用

1. 引入依赖

   ```
   //完整版引入
       implementation 'com.github.CarGuo.GSYVideoPlayer:gsyVideoPlayer:v8.1.5-jitpack'
   ```

   

2. 修改布局文件

   ```xml
       <com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer
           android:layout_width="match_parent"
           android:layout_height="200dp"
           android:id="@+id/video_player"/>
   ```

3.  编写java代码

   ```java
   public class MainActivity extends AppCompatActivity {
       StandardGSYVideoPlayer videoPlayer;
       OrientationUtils orientationUtils;
   
       @Override
       protected void onCreate(Bundle savedInstanceState) {
           super.onCreate(savedInstanceState);
           setContentView(R.layout.activity_main);
   
           init();
       }
   
       private void init() {
           videoPlayer =  (StandardGSYVideoPlayer)findViewById(R.id.video_player);
   
           String source1 = "https://media.w3.org/2010/05/sintel/trailer.mp4";
           videoPlayer.setUp(source1, true, "测试视频");
   
           //增加封面
   //        ImageView imageView = new ImageView(this);
   //        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
   //        imageView.setImageResource(R.mipmap.xxx1);
   //        videoPlayer.setThumbImageView(imageView);
           //增加title
           videoPlayer.getTitleTextView().setVisibility(View.VISIBLE);
           //设置返回键
           videoPlayer.getBackButton().setVisibility(View.VISIBLE);
           //设置旋转
           orientationUtils = new OrientationUtils(this, videoPlayer);
           //设置全屏按键功能,这是使用的是选择屏幕，而不是全屏
           videoPlayer.getFullscreenButton().setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   orientationUtils.resolveByClick();
               }
           });
           //是否可以滑动调整
           videoPlayer.setIsTouchWiget(true);
           //设置返回按键功能
           videoPlayer.getBackButton().setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   onBackPressed();
               }
           });
           videoPlayer.startPlayLogic();
       }
   
   
       @Override
       protected void onPause() {
           super.onPause();
           videoPlayer.onVideoPause();
       }
   
       @Override
       protected void onResume() {
           super.onResume();
           videoPlayer.onVideoResume();
       }
   
       @Override
       protected void onDestroy() {
           super.onDestroy();
           GSYVideoManager.releaseAllVideos();
           if (orientationUtils != null)
               orientationUtils.releaseListener();
       }
   
       @Override
       public void onBackPressed() {
           //先返回正常状态
           if (orientationUtils.getScreenType() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
               videoPlayer.getFullscreenButton().performClick();
               return;
           }
           //释放所有
           videoPlayer.setVideoAllCallBack(null);
           super.onBackPressed();
       }
   }
   ```

   

## 2、简单自定义UI 

1. 去GSYVideoPlayer，java包下的layout 文件中复video_layout_standard.xml到自己的布局文件

2. 自己新建一个java类JcPlayer,继承StandardGSYVideoPlayer

3. 修改自己的布局文件

   ```xml
   <LinearLayout
           android:layout_width="wrap_content"
           android:layout_height="wrap_content"
           android:orientation="vertical"
           android:layout_centerHorizontal="true"
           android:layout_centerVertical="true"
           android:gravity="center">
           <moe.codeest.enviews.ENDownloadView
               android:id="@+id/loading"
               android:layout_width="28dp"
               android:layout_height="28dp"
               android:visibility="invisible" />
           <TextView
               android:id="@+id/tv_netSpeed"
               android:visibility="invisible"
               android:textColor="@color/white"
               android:layout_width="wrap_content"
               android:layout_height="wrap_content" />
       </LinearLayout>
   ```

   

4. 在自己的类中重写一些方法

   ```java
   public class JcPlayer extends StandardGSYVideoPlayer {
       private TextView testText;
   
   
       public JcPlayer(Context context, Boolean fullFlag) {
           super(context, fullFlag);
       }
   
       public JcPlayer(Context context) {
           super(context);
       }
   
       public JcPlayer(Context context, AttributeSet attrs) {
           super(context, attrs);
       }
   
       @Override
       public int getLayoutId() {
           return R.layout.video_layout_cover;
       }
   
       @Override
       protected void init(Context context) {
           super.init(context);
           testText=findViewById(R.id.tv_netSpeed);
       }
   
       @Override
       protected void changeUiToPlayingShow() {
           super.changeUiToPlayingShow();
           testText.setText("哈哈");
           setViewShowState(testText,VISIBLE);
       }
   }
   ```

   